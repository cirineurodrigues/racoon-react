import styled from "styled-components";

import bannerBg from "../../assets/bannerBg.png";

export const StyledBanner = styled.section`
  height: calc(100vh - 60px);
  min-height: 400px;
  margin-top: 60px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 1)),
    url(${bannerBg});
  background-size: cover;
  background-position-x: 40%;

  @media screen and (min-width: 768px) {
    height: calc(100vh - 80px);
    min-height: 450px;
    margin-top: 80px;
  }
`;
