import { StyledHeader, Button } from "./styles";

import mainLogo from "../../assets/mainLogo.png";

const Header = ({ menuItems, btnText }) => {
  return (
    <StyledHeader>
      <div className="container">
        <img src={mainLogo} alt="Lorem Ipsum logo"></img>
        <nav>
          <div id="hamburguerMenu">
            <div className="stripe"></div>
            <div className="stripe"></div>
            <div className="stripe"></div>
          </div>
          <ul>
            {menuItems.map((value, index) => (
              <li key={index}>{value}</li>
            ))}
          </ul>
          <Button>{btnText}</Button>
        </nav>
      </div>
    </StyledHeader>
  );
};

export default Header;
