import styled from "styled-components";

export const StyledHeader = styled.header`
  width: 100%;
  height: 60px;
  position: fixed;
  top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
  background-color: #2571b7;

  img,
  ul,
  li {
    display: none;
  }

  .container {
    width: 90%;
    height: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  nav {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  #hamburguerMenu {
    width: 28px;
    height: 21px;
    margin-left: 5%;
    cursor: pointer;
  }

  .stripe {
    width: 100%;
    height: 2.5px;
    background-color: #fff;
  }

  .stripe:first-child {
    transition: 0.3s ease;
    transform: translateY(0);
  }

  .stripe:nth-child(2) {
    transition: 0.3s ease;
    transform: translateY(7px);
  }

  .stripe:last-child {
    transition: 0.3s ease;
    transform: translateY(14px);
  }

  @media screen and (min-width: 768px) {
    height: 80px;

    #hamburguerMenu {
      display: none;
    }

    img {
      display: block;
      width: 120px;
    }

    ul {
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: space-evenly;
      align-items: center;
    }

    li {
      height: 100%;
      display: flex;
      align-items: center;
      list-style: none;
      color: #fff;
      cursor: pointer;
    }

    li:hover {
      transition: 0.5s;
      color: #011129;
    }

    @media screen and (min-width: 992px) {
      ul {
        width: 60%;
        margin-left: 40%;
      }
    }
  }
`;

export const Button = styled.button`
  min-height: 30px;
  min-width: 120px;
  border: none;
  color: #fff;
  font-weight: bold;
  cursor: pointer;
  background-color: #002050;

  :hover {
    transition: 0.5s;
    background-color: #011129;
  }
`;
