import GlobalStyle from "./styles/global";

import Header from "./components/Header";
import Banner from "./components/Banner";

const App = () => {
  return (
    <>
      <GlobalStyle />
      <Header
        menuItems={["Lorem Ipsum", "Lorem Ipsum", "Lorem Ipsum"]}
        btnText="Lorem Ipsum"
      />
      <Banner
        title="Lorem ipsum"
        subTitle="Lorem ipsum dolor sit amet, consectetur adipisicing elit"
        btnText="Lorem ipsum"
      />
    </>
  );
};

export default App;
